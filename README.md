EDIUM 1 "MAY 2015" Linux "User Manuals"
=======================================

NAME
----

edium - simple TUI text editor

SYNOPSIS
--------

`edium` [`-hv`] [`--help`] [`--version`] *file* ...

DESCRIPTION
------------

A light vim and emacs based text editor for the terminal, still in extremely
heavy development and planning.

OPTIONS
-------

`-h` `--help`
  Display help

`-v` `--version`
  Display version

AUTHOR
------

<https://github.com/edvb54/edium>

ED <edvb54@gmail.com>

LICENSE
-------

GPL v3 License

