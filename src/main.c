#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>

#include "edium.h"
#include "file.h"

static const struct option longopts[] = {
	{"help", no_argument, NULL, 'h'},
	{"version", no_argument, NULL, 'v'},
	{NULL, 0, NULL, 0}
};

static void ratium_help(char *name) {
	printf("\
Usage: %s [OPTION]\n\
A small text editor\n\
\n\
  -h, --help      display this help and exit\n\
  -v, --version   display version information and exit\n\
\n\
See man page for more information\n\
\n\
edium home page: <https://github.com/edvb54/edium>\n\
", name);
	exit(EXIT_SUCCESS);
}

static void ratium_version(char *name) {
	printf("%s v%s - GPL v3\n", name, VERSION);
	exit(EXIT_SUCCESS);
}

int main(int argc, char *argv[]) {
	int optc;

	fileqty = 0;

	if (argc == 1) /* if there is no arguments, open enpty buffer */
		file_load(NULL);
	else
		for (int i = 1; i < argc; i++)
			if (argv[i][0] == '-') /* handle options */
				while ((optc = getopt_long(argc, argv, "hv", longopts, NULL)) != -1)
					switch (optc) {
					case 'h':
						ratium_help(argv[0]);
					case 'v':
						ratium_version(argv[0]);
					default:
						exit(EXIT_FAILURE);
					}
			else /* open file given */
				file_load(argv[i]);

	printf("%d\n", fileqty);
	return EXIT_SUCCESS;
}

