#ifndef EDIUM_H
#define EDIUM_H

typedef enum { false, true } bool;

typedef struct _Line Line;
struct _Line {
	char *c;
	size_t len;
	Line *next;
	Line *prev;
};

typedef struct _File File;
struct _File {
	char *fname;
	Line *fstline;
};

File filebuf[5];
int fileqty;

#endif /* EDIUM_H */
