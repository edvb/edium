#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "edium.h"
#include "file.h"

void file_load(char *fname) {
	if (fname == NULL) /* open enpty buffer */
		printf("loading blank file\n");
	else {
		filebuf[fileqty].fname = malloc(strlen(fname));
		strcpy(filebuf[fileqty].fname , fname);
		printf("loading file %s\n", filebuf[fileqty].fname);
	}

	fileqty++;
}
